(function($){
  Backdrop.behaviors.jquery_coundown_timer_init_popup = {
    attach: function(context, settings) {
      var note = $('#jquery-countdown-timer-note'),
      ts = new Date(Backdrop.settings.jquery_countdown_timer.jquery_countdown_timer_date * 1000);
      $('#jquery-countdown-timer').not('.jquery-countdown-timer-processed').addClass('jquery-countdown-timer-processed').countdown({
	timestamp : ts,
	callback : function(weeks,days, hours, minutes, seconds){
          var dateStrings = new Array();
          dateStrings['@weeks'] = Backdrop.formatPlural(weeks, '1 week', '@count weeks');
          dateStrings['@days'] = Backdrop.formatPlural(days, '1 day', '@count days');
          dateStrings['@hours'] = Backdrop.formatPlural(hours, '1 hour', '@count hours');
          dateStrings['@minutes'] = Backdrop.formatPlural(minutes, '1 minute', '@count minutes');
          dateStrings['@seconds'] = Backdrop.formatPlural(seconds, '1 second', '@count seconds');
          var message = Backdrop.t('@weeks, @days, @hours, @minutes and @seconds left', dateStrings);
          note.html(message);
        }
      });
    }
  }
})(jQuery);
